// write function here
const sumArray = (Angka) => {
  let sum = 0;
  let pertambahan = 0;
  for (let i = 0; i < Angka.length; i++){
    sum = Angka[i]+pertambahan;
    pertambahan = sum 
  }
  return sum;
};

// input test
const input1 = [1, 2, 5, 8, 9, 10];
const input2 = [1, 2, 3, 4, 5];

sumArray(input1); // output: 35
sumArray(input2); // output: 15
