// write function here
const printNumberDescending = (data) => {
    for (let i = data; i >= 0; i--) {
      console.log(i);
    }
    return;
  };
  
  // input test
  const input1 = 5;
  const input2 = 10;
  
  printNumberDescending(input1); // output: 0 1 2 3 4 5
  //printNumberDescending(input2);